package CEST.PET;

public abstract class Animal {
	public String nome;
	public int idade;
	public TipoRaca raca;
	public boolean doente; 
	
	public void dormir() {
		System.out.println("zzzz");
	}
	
	public void fazerbarulholatir() {
		System.out.println("auau");
	}
	
	public void fazerbarulhomiar() {
		System.out.println("miau");
	}
	
	public void procurarComida() {
		System.out.println("rolar");
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public int getIdade() {
		return idade;
	}

	public void setIdade(int idade) {
		this.idade = idade;
	}

	public TipoRaca getRaca() {
		return raca;
	}

	public void setRaca(TipoRaca raca) {
		this.raca = raca;
	}

	public boolean isDoente() {
		return doente;
	}

	public void setDoente(boolean doente) {
		this.doente = doente;
	}
	

}
